# wallpaperScript

* A script to download wallpapers from /r/wallpapers
* Put the script in your /etc/cron.{daily/weekly/monthly} folder
* The wallpaper changing script can be started in the background during startup
