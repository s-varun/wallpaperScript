#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
	from urllib.request import urlopen
	from urllib.error import HTTPError
except ImportError:
	from urllib2 import urlopen
	from urllib2 import HTTPError
import time
import os
import json
import socket

class Configuration:
	def __init__(self, url, directory, min_score=200, min_width=1920, min_height=1080, desired_aspect_ratio=1.78, nsfw=False,
				max_attempts=10, sleep_seconds_after_attempt=2, timeout = 3):
		self.url = url
		self.directory = directory
		self.min_width = min_width
		self.min_height = min_height
		self.max_attempts = max_attempts
		self.sleep_seconds_after_attempt = sleep_seconds_after_attempt
		self.desired_aspect_ratio = desired_aspect_ratio
		self.min_score = min_score
		self.nsfw = nsfw
		self.timeout = timeout

configs = [Configuration('https://www.reddit.com/r/wallpapers/top.json?t=week&limit=50', '/home/varun/Pictures/RedditWallpapers/'),
			]


class Image:
	def __init__(self, title, url, width, height, nsfw, score):
		self.title = title
		self.url = url
		self.width = width
		self.height = height
		self.nsfw = nsfw
		self.score = score

	def __str__(self):
		string  = "Title: " + self.title + "\n"
		string += "	URL: " + self.url + "\n"
		string += "	Width: " + str(self.width) + "\n"
		string += "	Height: " + str(self.height) + "\n"
		string += "	NSFW: " + str(self.nsfw) + "\n"
		string += "	Score: " + str(self.score)  + "\n\n"
		return string.encode('ascii', 'ignore')

class Downloader:
	def __init__(self, config):
		self.config = config

	def get_validity(self, image):
		# Check if URL exists
		if image.url is None:
			return False

		# Check min resolution
		if not (image.width >= self.config.min_width and image.height >=self.config.min_height):
			return False

		# Check aspect ratio
		ratio = float("{0:.2f}".format(image.width/float(image.height)))
		if abs(ratio - self.config.desired_aspect_ratio) < 0.01:
			if image.score >= self.config.min_score and image.nsfw is self.config.nsfw:
				print image.title
				return True
		return False

	def save_image(self, url, file_path):

		f = open(file_path, 'wb')

		i = 0
		while True:
			if i == self.config.max_attempts:
				f.close()
				raise Exception('Sorry, can\'t reach imgur.')
			try:
				data = urlopen(url, timeout=self.config.timeout).read()
				if len(data) > 0:
					f.write(data)
				else:
					raise Exception('0 Bytes in download, exiting')
				f.close()
				break
			except HTTPError:
				time.sleep(1)
				i += 1
			except socket.timeout:
				# Socket timeout, try again.
				i += 1

	def get_images(self, url, desired_res=None):
		i = 0
		while True:
			if i == self.config.max_attempts:
				raise Exception('Sorry, can\'t reach reddit.')
			try:
				data = json.loads(
					urlopen(url, timeout=self.config.timeout).read().decode('utf-8'))
				break
			except HTTPError as e:
				# Too many requests, give reddit a break, try again.
				print("JSON api throttled, attempt %s on %s" % (i, self.config.max_attempts))
				if getattr(e, 'code', None) == 429:
					time.sleep(self.config.sleep_seconds_after_attempt)
				i += 1
			except socket.timeout:
				print("Timeout, attempt %s on %s" % (i, self.config.max_attempts))
				time.sleep(self.config.sleep_seconds_after_attempt)
				i += 1

		images = []

		# Extract information from json
		for item in data.get('data', {}).get('children', {}):
			title = item.get('data', {}).get('title', '')
			score = item.get('data', {}).get('score', '')
			nsfw = False
			if item.get('data', {}).get('over_18','') == 'true':
				nsfw = True
			for image in item.get('data', {}).get('preview', {}).get('images', {}):
				source = image.get('source','')
				my_img = Image(title,
						  source.get('url',''),
						  source.get('width',''),
						  source.get('height',''),
						  nsfw,
						  score )
				images.append(my_img)

		return images


if __name__ == '__main__':
	for config in configs:
		downloader = Downloader(config)
		images = downloader.get_images(config.url)
		for image in images:
			if (downloader.get_validity(image)):
				# Some titles have a slash which should be removed
				file_path = config.directory + image.title.replace('/',' ')

				# Don't download if file exists
				if not os.path.exists(file_path):
					print image
					downloader.save_image(image.url,file_path)
