#!/usr/bin/env python
import os, random
from time import sleep

wallpaperDir = "/home/varun/Pictures/RedditWallpapers/"
changeInterval = 600 # in seconds

def changeWallpaper():
    randomFile = random.choice(os.listdir(wallpaperDir)) #change dir name to whatever
    os.system("gsettings set org.gnome.desktop.background picture-uri 'file://" + wallpaperDir + randomFile + "'")

while(True):
    changeWallpaper()
    sleep(changeInterval)
